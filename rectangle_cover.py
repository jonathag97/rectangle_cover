import copy
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
import heapq
import copy

class Point:
    def __init__(self, a=0, b=0, random=False):
		#generates a random point with 4 digits at maximum
		#Every 4 digit integer "abcd"" will be converted to "0,abcd"" when calculating the area
        if random:
            coordinates = np.random.randint(0, 10000, 2)
            if coordinates[0] == 1 and coordinates[1] == 0:
                coordinates = [0, 1]
            self.coordinates = list(coordinates)
        else:
            self.coordinates = [a, b]
		#every point dynamically manages its tile with possible corner points
		
        self.tile = []
		# every corner is mapped to the corresponding rectangle
        self.tile_map = dict()
        self.fix_points = set()
		# list of points, whos rectangle choice could change the tile
        self.notifiers = []
        self.tile_width = 10000 - self[0]
        self.tile_height = 10000 - self[1]
        self.tiled = False
        self.rectangle = None

    def __getitem__(self, key):
        return self.coordinates[key]

    # comparison operators are applied to both tuple-entries
    def __le__(self, other):
        return np.sum(self.coordinates) <= np.sum(other.coordinates)

    def __gt__(self, other):
        return np.sum(self.coordinates) > np.sum(other.coordinates)

    def __ge__(self, other):
        return np.sum(self.coordinates) >= np.sum(other.coordinates)

    def __lt__(self, other):
        return np.sum(self.coordinates) < np.sum(other.coordinates)

    def __eq__(self, other):
        return self.coordinates == other.coordinates

    def __ne__(self, other):
        return not self == other

    def dominates(self, other):
        return self[0] >= other[0] and self[1] >= other[1]

    def set_rectangle(self, rec):
        self.rectangle = rec

    def get_rectangle(self):
        return self.rectangle

    def set_edge(self, point):
        self.tile.append(point)

    def set_tile(self):
        # in place sorting, for same y value they are sorted by their x value
        self.tile.sort(key=lambda tup: tup[0])
        self.tile.sort(key=lambda tup: tup[1])
        if len(self.tile) > 0:
            tmp = self.tile[-1]
		# if two points, which possibly bound the tile, have the same height,
		# then the point further to the right can be ignored
        for index in range(len(self.tile) - 2, -1, -1):
            point = self.tile[index]
            if point[1] == tmp[1]:
                self.tile.pop(index)
            else:
                tmp = point
        last = 10000
		#every corner point depends on the x-value of the previous tile-point
        for point in self.tile:
			#generate corner and the corresponding rectangle
            corner = (last, point[1])
            rct = Rectangle(self.coordinates, corner)
            self.tile_map[corner] = rct
            self.fix_points.add((rct.area, corner))
            last = point[0]
		#remaining corner, which is at the very top of the unit square
        corner = (last, 10000)
        rct = Rectangle(self.coordinates, corner)
        self.tile_map[corner] = rct
        self.fix_points.add((rct.area, corner))
        return
	#when a point chooses a rectangle, he notifies all points depending on him
    def point_notify(self, point, corner):
        # case where the independent point limits the tile to the right
        if point[0] >= self[0] and corner[1] > self[1] and point[0] < self[0] + self.tile_width:
            min_x = (10000, 10000)
            fixpoint_copy = copy.copy(self.fix_points)
			#remove all fixpoints that are now invalid
            for fixpoint in fixpoint_copy:
                area = fixpoint[0]
                fixpoint = fixpoint[1]
                if fixpoint[0] >= point[0]:
                    self.fix_points.remove((area, fixpoint))
                    if fixpoint[0] <= min_x[0]:
                        min_x = fixpoint
			#a new corner point is now the rightmost
            new_point = (point[0], min_x[1])
            rct = Rectangle(self, new_point)
            self.tile_map[new_point] = rct
            self.fix_points.add((rct.area, new_point))
            self.tile_width = new_point[0] - self[0]
		# case where the independent point limits the tile to the top
        elif point[1] >= self[1] and corner[0] > point[0] and point[1] < self[1] + self.tile_height:
            min_y = (10000, 10000)
            fixpoint_copy = copy.copy(self.fix_points)
			#remove all fixpoints that are now invalid
            for fixpoint in fixpoint_copy:
                area = fixpoint[0]
                fixpoint = fixpoint[1]
                if fixpoint[1] >= point[1]:
                    self.fix_points.remove((area, fixpoint))
                    if fixpoint[1] <= min_y[1]:
                        min_y = fixpoint
			#a new corner point is now the topmost
            new_point = (min_y[0], point[1])
            rct = Rectangle(self, new_point)
            self.tile_map[new_point] = rct
            self.fix_points.add((rct.area, new_point))
            self.tile_height = new_point[1] - self[1]

    def choose(self):
        max_area = 0
        max_fp = self.coordinates
        max_rct = Rectangle(self, self)
		#iterate through all fixpoints and choose the fixpoint with minimal area
        assert len(self.fix_points) > 0
        for fix_pt in self.fix_points:
            fix_pt = fix_pt[1]
            rct = self.tile_map[fix_pt]
            area = rct.area()
            if area > max_area:
                max_area = rct.area()
                max_fp = fix_pt
                max_rct = Rectangle(self, max_fp)
        self.set_rectangle(max_rct)
        for point in self.notifiers:
            point.point_notify(self, max_fp)
        return max_area


class Square:

    def __init__(self, point_amount=0, point_list=[]):
        self.point_amount = point_amount - 1
        self.pointlist = [Point()]
		# case for random points
        if not point_list:
            for i in range(self.point_amount):
                point = Point(random=True)
                self.pointlist.append(point)
		# case where points are given
        else:
            self.pointlist = []
            for p in point_list:
                point = Point(p[0], p[1])
                self.pointlist.append(point)
        self.point_amount = len(point_list)

    def set_tiles(self):
        for point in self.pointlist:
            point.set_tile()
        return


class Rectangle:
    def __init__(self, point_left_down, point_right_up):
        self.point_left_up = (point_left_down[0], point_right_up[1])
        self.point_left_down = point_left_down
        self.point_right_up = point_right_up
        self.width = point_right_up[0] - point_left_down[0]
        self.height = point_right_up[1] - point_left_down[1]
        self.left_neighbor = None

    def overlap(self, other):
        if other is None:
            return False
        x1 = self.point_left_down[0]
        y1 = self.point_left_down[1]
        x2 = other.point_left_down[0]
        y2 = other.point_left_down[1]
        # check if one rectangle is at the left of the other
        if x2 >= x1 + self.width or x1 >= x2 + other.width:
            return False
        # check if one rectangle is above the other
        elif y2 >= y1 + self.height or y1 >= y2 + other.height:
            return False
        else:
            return True
	# checks if a point is inside
	# it is only inside if it does not touch the bounding lines
    def inside(self, point2):
        x1 = self.point_left_down[0]
        y1 = self.point_left_down[1]
        x2 = point2[0]
        y2 = point2[1]
        return x1 < x2 < x1 + self.width and y1 < y2 < y1 + self.height
	# checks if a point is inside
	# it is also inside if it touches the bounding lines
    def soft_inside(self, point2):
        x1 = self.point_left_down[0]
        y1 = self.point_left_down[1]
        x2 = point2[0]
        y2 = point2[1]
        return x1 <= x2 <= x1 + self.width and y1 <= y2 <= y1 + self.height

    def area(self):
        return self.width * self.height

    def __ge__(self, other):
        y1 = self.point_left_down[1]
        y2 = other.point_left_down[1]
        return y1 >= y2




def preparations(c, sort=True):
    if sort:
        c.pointlist.sort(reverse=True)
	# every point gets a list of points he depends on for choosing the rectangle
    dependence(c.pointlist)
    tmp = c.pointlist.copy()
    tmp.sort()
    origin = tmp.pop(0)
	# sets edges from one point to all its tile-bounding points
    acyclic_graph(origin, tmp)
    c.set_tiles()
    return


def greedy(pts=[], random=False, amount=10):
    # generates points
    if random:
        c = Square(point_amount=amount)
    else:
        c = Square(point_list=pts)
    # make initialisation
    preparations(c)
    covered_area = 0
    for point in c.pointlist:
        max_area = greedy_step(point)
        covered_area += max_area
    return c.pointlist, covered_area / 100000000


def greedy_step(point):
    max_area = point.choose()
    return max_area

def points_to_file(points, area, file):
    file.write(points_to_string(points) + str(area) + "\n")


def points_to_string(pts):
    string = ""
    for point in pts:
        string += str(point.coordinates) + ";"
    return string

def greedy_tool(amount, confidence):
    with open(str(amount) + '_points.txt', 'w') as the_file, open(str(amount) + "_" + str(confidence) + '.txt',
                                                                  'w') as file2:
        if confidence == 1:
            maxiter = 10
        elif confidence == 2:
            maxiter = 100
        elif confidence == 3:
            maxiter = 1000
        else:
            return
        means = []
        sds = []
        lows = []
        highs = []
        most = []
        areas = []
        # initialize lowest with maximal area
        lowest = 100000000
        # initialize highest with minimal area
        highest = 0
        for iterations in tqdm(range(maxiter)):
            rand_points, area = greedy(random=True, amount=amount)
            if area < lowest:
                lowest = area
            if area > highest:
                highest = area
            areas.append(area)
            the_file.write(points_to_string(rand_points) + str(area) + "\n")
        mean = np.mean(areas)
        sd = np.std(areas)
        median = np.median(areas)
        areas = np.array(areas)
        areas *= 100
        areas = np.rint(areas)
        areas = areas.astype(int)
        counts = np.bincount(areas)
        most_common = np.argmax(counts) / 100
        most.append(most_common)
        means.append(mean)
        sds.append(sd)
        lows.append(lowest)
        highs.append(highest)
        file2.write("size;mean;sd;median;most_common;lowest;highest\n")
        file2.write(
            str(amount) + ';' + str(mean) + ';' + str(sd) + ';' + str(median) + ';' + str(most_common) + ';' + str(
                lowest) + ';' + str(highest) + '\n')

def acyclic_graph(point, points):
    interest = copy.copy(points)
    point.tiled = True
    while interest:
        point2 = interest[0]
        point.set_edge(point2)
        dominators = [point3 for point3 in points if point3.dominates(point2)]
        interest = [point for point in interest if point not in dominators]
        dominators.pop(dominators.index(point2))
        if not point2.tiled:
            acyclic_graph(point2, copy.copy(dominators))


def dependence(points):
    for index in range(len(points) - 1):
        # points are sorted in reversed order
        point = points[-1 - index]
        # point is dependent on points, where there is no domination and that choose a rectangle before point
        dependent = [point2 for point2 in points[:(-1 - index)] if
                     not (point.dominates(point2) or point2.dominates(point))]
        for point2 in dependent:
            point2.notifiers.append(point)


